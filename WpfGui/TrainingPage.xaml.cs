﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace BiaiProject
{
    /// <summary>
    /// Interaction logic for TrainingPage.xaml
    /// </summary>
    public partial class TrainingPage : UserControl
    {
        private Thread thread;

        // createCNN.py parameters
        private string datasetPath;
        private bool horizontalFlip = true;
        private float rotationRange = 10.0f;
        private float zoomRange = 0.1f;

        public TrainingPage()
        {
            InitializeComponent();
        }

        private void CreateCNNButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(datasetPath))
            {
                if (thread == null || !thread.IsAlive)
                {
                    loadingSpinner.Visibility = Visibility.Visible;
                    thread = new Thread(new ThreadStart(CreateCNN));
                    thread.Start();
                }
            }
        }

        private void CreateCNN()
        {
            var psi = new ProcessStartInfo();
            psi.FileName = @"C:\Users\Michal PC\AppData\Local\Programs\Python\Python37\python.exe"; // link do pythona
            var script = @"D:\biaiproject\pythonCNN\createCNN.py"; // link do naszego skryptu
            psi.Arguments = $"\"{script}\" \"{datasetPath}\" \"{horizontalFlip.ToString()}\" \"{rotationRange.ToString()}\" \"{zoomRange.ToString()}\"";
            psi.UseShellExecute = false;
            psi.RedirectStandardOutput = false;
            psi.RedirectStandardError = false;
            psi.CreateNoWindow = false;
            var process = Process.Start(psi);
            while (true)
            {
                if (process.HasExited)
                {
                    Dispatcher.Invoke(() =>
                    {
                        loadingSpinner.Visibility = Visibility.Hidden;
                    });
                    return;
                }
            }
        }

        private void setDatasetPathButton_Click(object sender, RoutedEventArgs e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                datasetPath = dialog.FileName;
                datasetPathTextBlock.Text = datasetPath;
            }
        }

        private void radioButton1_Checked(object sender, RoutedEventArgs e) => horizontalFlip = true;
        private void radioButton2_Checked(object sender, RoutedEventArgs e) => horizontalFlip = false;

        private void rotationRangeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            rotationRange = float.Parse(rotationRangeTextBox.Text);
        }

        private void zoomRangeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            zoomRange = float.Parse(zoomRangeTextBox.Text);
        }
    }
}

